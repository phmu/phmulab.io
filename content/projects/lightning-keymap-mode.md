---
title: "Lightning-keymap mode for Emacs"
date: "2017-11-18T17:39:21-07:00"
description: "https://github.com/theGreatWhiteShark/lightning-keymap-mode"
---
An Emacs minor mode providing a lightning-fast keymap 
