---
title: "The Climex R package + web app"
date: "2017-12-09T17:39:21-07:00"
description: "https://github.com/theGreatWhiteShark/climex"
---

Extreme value analysis on climatic time series using R and Shiny.
